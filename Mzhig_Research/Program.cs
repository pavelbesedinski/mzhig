﻿using System;
using Mzhig_Research.MathService;

namespace Mzhig_Research
{
    class Program
    {
        static void Main(string[] args)
        {
            Task1 task1 = new Task1();

            task1.ExplicitLeftCornerSolverSolution_1();
            task1.ExplicitLeftCornerSolverSolution_2();
            task1.ExplicitLeftCornerSolverSolution_3();

            task1.ExplicitFourthPointSolverSolution_1();
            task1.ExplicitFourthPointSolverSolution_2();
            task1.ExplicitFourthPointSolverSolution_3();

            task1.ExplicitRightCornerSolverSolution_1();
            task1.ExplicitRightCornerSolverSolution_2();
            task1.ExplicitRightCornerSolverSolution_3();

            task1.ImplicitFourthPointSolverSolution_1();
        }
    }
}
