﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    public abstract class ComputationalArea : ISolver
    {
        /// <summary>
        /// Размер области 
        /// </summary>
        internal readonly double AreaSize;

        /// <summary>
        /// Количество точек вдоль оси
        /// </summary>
        internal readonly int N;

        /// <summary>
        /// Шаг вдоль оси
        /// </summary>
        internal readonly double h;

        /// <summary>
        /// Шаг по времени
        /// </summary>
        internal readonly double tau;

        /// <summary>
        /// Кожффициент
        /// </summary>
        internal readonly double сoefficient;

        /// <summary>
        /// Массив значений расчётной области
        /// </summary>
        internal double[] U;

        /// <summary>
        /// Обновленный массив значений расчётной области
        /// </summary>
        internal double[] Unew;

        /// <summary>
        /// Нижняя временная граница
        /// </summary>
        internal double timeMin;

        /// <summary>
        /// Верхняя временная граница
        /// </summary>
        internal double timeMax;


        /// <summary>
        /// Создать расчётную область
        /// </summary>
        /// <param name="AreaSize">Размер области</param>
        /// <param name="h">Шаг по области</param>
        /// <param name="tau">Шаг по времени</param>
        /// <param name="сoefficient">Коэффициент</param>
        /// <param name="timeMin">Нижняя временная граница</param>
        /// <param name="timeMax">Верхняя ременная граница</param>
        public ComputationalArea(double AreaSize, double h, double tau, double сoefficient, double timeMin, double timeMax)
        {
            this.AreaSize = AreaSize;
            this.h = h;
            this.tau = tau;
            this.сoefficient = сoefficient;
            this.timeMin = timeMin;
            this.timeMax = timeMax;

            N = (int)(AreaSize / h) + 1;

            U = new double[N];
            Unew = new double[N];

            Random random = new Random(new DateTime().Second);
            for (int i = 0; i < N; i++)
            {
                U[i] = Unew[i] = random.NextDouble();
            }
        }

        internal double Courant => сoefficient * tau / h;
        public abstract double[] Solve();
    }
}
