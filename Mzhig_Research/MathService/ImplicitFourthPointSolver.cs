﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    class ImplicitFourthPointSolver : ComputationalArea
    {
        private double[] L;
        private double[] Lnew;
        private double[] M;
        private double[] Mnew;

        double R => Courant / 2;

        public ImplicitFourthPointSolver(double AreaSize, double h, double tau, double сoefficient, double timeMin, double timeMax) : base(AreaSize, h, tau, сoefficient, timeMin, timeMax)
        {
            int uLength = U.Length;
            L = new double[uLength];
            Lnew = new double[uLength];
            M = new double[uLength];
            Mnew = new double[uLength];

            Random random = new Random(new DateTime().Millisecond);
            for (int i = 0; i < uLength; i++)
            {
                L[i] = M[i] = Lnew[i] = Mnew[i] = random.NextDouble();
            }

            L[0] = 0.0;
            M[0] = Unew[uLength - 1];
        }

        /// <summary>
        /// Расчет нового значения точки
        /// </summary>
        /// <param name="uFutureRight">U(i+1, k+1)</param>
        /// <param name="uCurrentCenter">U(i, k)</param>
        /// <param name="centerL">L(i)</param>
        /// <param name="centerM">M(i)</param>
        /// <returns>U(i, k+1)</returns>
        private double Function(double uFutureRight, double uCurrentCenter, double centerL, double centerM)
        {
            double value = FunctionL(centerL) * uFutureRight + FunctionM(centerM, centerL, uCurrentCenter);
            return value;
        }

        /// <summary>
        /// Найти новое значение коэффициента L
        /// </summary>
        /// <param name="centerL">L(i)</param>
        /// <returns>L(i+1)</returns>
        private double FunctionL(double centerL)
        {
            return -R / (1 - R * centerL);
        }

        /// <summary>
        /// Найти новое значение коэффициента M
        /// </summary>
        /// <param name="centerM">M(i)</param>
        /// <param name="centerL">L(i)</param>
        /// <param name="uCurrentCenter">U(i, k)</param>
        /// <returns>M(i+1)</returns>
        private double FunctionM(double centerM, double centerL, double uCurrentCenter)
        {
            return (uCurrentCenter + R * centerM) / (1 - R * centerL);
        }

        public override double[] Solve()
        {
            U[0] = Unew[0] = 10;
            do
            {
                for (int i = 1; i < N - 1; i++)
                {
                    Unew[i] = Function(Unew[i + 1], U[i], L[i], M[i]);
                }
                for (int i = 1; i < N - 1; i++) {
                    Lnew[i] = FunctionL(L[i]);
                }
                for (int i = 1; i < N - 1; i++)
                {
                   Mnew[i] = FunctionM(M[i], L[i], U[i]);
                }
                timeMin += tau;
                for (int i = 0; i < N; i++)
                {
                    U[i] = Unew[i];
                    L[i] = Lnew[i];
                    M[i] = Mnew[i];
                }
            } while (timeMin <= timeMax);
            return (double[])U.Clone();
        }
    }
}
