﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    class ExplicitRightCornerSolver : ComputationalArea
    {
        public ExplicitRightCornerSolver(double AreaSize, double h, double tau, double сoefficient, double timeMin, double timeMax) : base(AreaSize, h, tau, сoefficient, timeMin, timeMax)
        {
        }

        /// <summary>
        /// Расчёт нового значения точки
        /// </summary>
        /// <param name="uCurrentCenter">U(i, k)</param>
        /// <param name="uCurrentRight">U(i+1, k)</param>
        /// <returns>Новое значение центральной точки U(i, k+1)</returns>
        private double Function(double uCurrentCenter, double uCurrentRight)
        {
            double value = uCurrentCenter - (сoefficient * tau) / h * (uCurrentRight - uCurrentCenter);
            return value;
        }
        public override double[] Solve()
        {
            Random random = new Random();
            U[0] = 0.01;
            for (int i = 0; i < N; i++)
            {
                U[i] = random.NextDouble();
            }
            do
            {
                for (int i = 1; i < N - 1; i++)
                {
                    Unew[i] = Function(U[i], U[i + 1]);
                }
                timeMin += tau;
                for (int i = 0; i < N; i++)
                {
                    U[i] = Unew[i];
                }
            } while (timeMin <= timeMax);
            return (double[])U.Clone();
        }
    }
}
