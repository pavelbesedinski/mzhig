﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    interface ISolver
    {
        double[] Solve();
    }
}
