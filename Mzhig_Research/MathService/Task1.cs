﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    public class Task1
    {
        double timeMin = 0.0;
        double timeMax = 3.0;
        /// <summary>
        /// Явный левый уголок для r = 1
        /// </summary>
        public void ExplicitLeftCornerSolverSolution_1()
        {
            double L = 1;
            double tau = 0.01;
            double h = 0.01;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitLeftCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        /// <summary>
        /// Явный левый уголок для r = 0.5
        /// </summary>
        public void ExplicitLeftCornerSolverSolution_2()
        {
            double L = 1;
            double tau = 0.001;
            double h = 0.02;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitLeftCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        /// <summary>
        /// Явный левый уголок для r = 1.01
        /// </summary>
        public void ExplicitLeftCornerSolverSolution_3()
        {
            double L = 1;
            double tau = 0.0101;
            double h = 0.01;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitLeftCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        public void ExplicitFourthPointSolverSolution_1()
        {
            double L = 1;
            double tau = 0.01;
            double h = 0.01;
            double c = 1.0;
            double q = 0.5;

            ComputationalArea solver
                = new ExplicitFourthPointSolver(L, h, tau, c, timeMin, timeMax, q);

            var result = solver.Solve();
        }

        public void ExplicitFourthPointSolverSolution_2()
        {
            double L = 1;
            double tau = 0.001;
            double h = 0.05;
            double c = 1.0;
            double q = 0.5;

            ComputationalArea solver
                = new ExplicitFourthPointSolver(L, h, tau, c, timeMin, timeMax, q);

            var result = solver.Solve();
        }

        public void ExplicitFourthPointSolverSolution_3()
        {
            double L = 1;
            double tau = 0.0101;
            double h = 0.01;
            double c = 1.0;
            double q = 0.5;

            ComputationalArea solver
                = new ExplicitFourthPointSolver(L, h, tau, c, timeMin, timeMax, q);

            var result = solver.Solve();
        }

        public void ExplicitRightCornerSolverSolution_1()
        {
            double L = 1;
            double tau = 0.01;
            double h = 0.01;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitRightCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        public void ExplicitRightCornerSolverSolution_2()
        {
            double L = 1;
            double tau = 0.001;
            double h = 0.05;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitRightCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        public void ExplicitRightCornerSolverSolution_3()
        {
            double L = 1;
            double tau = 0.0101;
            double h = 0.01;
            double c = 1.0;

            ComputationalArea solver
                = new ExplicitRightCornerSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }

        public void ImplicitFourthPointSolverSolution_1()
        {
            double L = 1;
            double tau = 0.001;
            double h = 0.03;
            double c = 1.0;

            ComputationalArea solver
                = new ImplicitFourthPointSolver(L, h, tau, c, timeMin, timeMax);

            var result = solver.Solve();
        }
    }
}
