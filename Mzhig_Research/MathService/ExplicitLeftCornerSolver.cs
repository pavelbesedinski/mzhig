﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    public class ExplicitLeftCornerSolver : ComputationalArea
    {
        public ExplicitLeftCornerSolver(double AreaSize, double h, double tau, double сoefficient, double timeMin, double timeMax) : base(AreaSize, h, tau, сoefficient, timeMin, timeMax)
        {
        }


        /// <summary>
        /// Функция расчёта нового значения
        /// </summary>
        /// <param name="uCurrentCenter">U(i, k)</param>
        /// <param name="uCurrentLeft">U(i-1, k)</param>
        /// <param name="courant">Число Куранта</param>
        /// <returns></returns>
        private double Function(double uCurrentCenter, double uCurrentLeft, double courant)
        {
            var value = uCurrentCenter - courant * (uCurrentCenter - uCurrentLeft);
            return value;
        }


        public override double[] Solve()
        {
            Random random = new Random();
            U[0] = 10;
            for (int i = 0; i < N; i++)
            {
                U[i] = random.NextDouble();
            }
            do
            {
                for (int i = 1; i < N-1; i++)
                {
                    Unew[i] = Function(U[i], U[i - 1], Courant);
                }
                timeMin += tau;
                for (int i = 0; i < N; i++)
                {
                    U[i] = Unew[i];
                }
            } while (timeMin <= timeMax);
            return (double[])U.Clone();
        }
    }
}
