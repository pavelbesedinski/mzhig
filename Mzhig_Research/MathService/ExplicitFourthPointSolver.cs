﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzhig_Research.MathService
{
    class ExplicitFourthPointSolver : ComputationalArea
    {
        private double q;

        public ExplicitFourthPointSolver(double AreaSize, double h, double tau, double сoefficient, double timeMin, double timeMax, double q) : base(AreaSize, h, tau, сoefficient, timeMin, timeMax)
        {
            this.q = q;
        }


        /// <summary>
        /// Функция расчёта нового значения
        /// </summary>
        /// <param name="uCurrentCenter">U(i, k)</param>
        /// <param name="uCurrentRight">U(i + 1, k)</param>
        /// <param name="uCurrentLeft">U(i - 1, k)</param>
        /// <returns>Новое значение точки U(i, k + 1)</returns>
        private double Function(double uCurrentCenter, double uCurrentRight, double uCurrentLeft)
        {
            var value = uCurrentCenter - (сoefficient * tau) / (2 * h) * (uCurrentRight - uCurrentLeft) + Math.Pow(tau, 2) / Math.Pow(h, 2) * q * (uCurrentLeft - 2 * uCurrentCenter + uCurrentRight);
            return value;
        }
        public override double[] Solve()
        {
            Random random = new Random();
            U[0] = 10;
            for (int i = 0; i < N; i++)
            {
                U[i] = random.NextDouble();
            }
            do
            {
                for (int i = 1; i < N - 1; i++)
                {
                    Unew[i] = Function(U[i], U[i + 1], U[i - 1]);
                }
                timeMin += tau;
                for (int i = 0; i < N; i++)
                {
                    U[i] = Unew[i];
                }
            } while (timeMin <= timeMax);
            return (double[])U.Clone();
        }
    }
}
